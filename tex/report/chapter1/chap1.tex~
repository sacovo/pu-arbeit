\chapter{Einleitung}
  \section{Problem/Frage/Hypothese}
  
  Vor der Industrialisierung und Modernisierung der Landwirtschaft 
  sah die Landschaft im Schweizer Mittelland wesentlich anderst aus. 
  Besonders das Freiamt war mit der mäandrierenden Reuss ein sehr 
  feuchtes Gebiet. Sümpfe und Moore prägten die Landschaft, speziell 
  für Amphibien waren die Bedingungen sehr hervorragend.
  Mit der Begradigung der Fluss- und Bachläufe, wurde das Flachland entsumpft. 
  Damit verkleinerte sich der Lebensraum der Amphibien enorm, es dauerte nicht lange,
  bis verschiedenste Arten eine Seltenheit wurden. Damit die kleinen Kaltblüter bei uns nicht ganz aussterben, muss ihnen gezielt
  mit Schutzgebieten geholfen werden.
  \\Nun stellt sich die Frage, ob in unserer dicht bebauten Landschaft überhaupt noch Platz ist für solche neuen Schutzgebiete.
  Mit dieser Arbeit wollen wir speziell die Frage "wo" klären, in dem wir mit Hilfe von GIS verschiedene Kriterien für 
  Amphibienlebensräume mit einander verknüpfen und damit neue Biotope ausfindig machen. 
  Wir sind davon überzeugt, dass namentlich im Freiamt geeignete Gebiete vorhanden sind.
  
  \section{Untersuchungsregion: Freiamt}
  Das Freiamt umfasst die Bezirke Bremgarten und Muri. %http://de.wikipedia.org/wiki/Freiamt_%28Schweiz%29
  Dieses Gebiet eignet sich gut als Untersuchungsregion, da die Reussebene Zuhause seltener Amphibienarten ist. Dies ist wichtig, 
  weil ein neues Schutzgebiet nur besiedelt werden kann, wenn das gewünschte Amphieb in einem Umkreis von 2 km bereits lebt.
  Zudem bietet das Freiamt mit den Flüssen Reuss und Bünz und zahlreichen anderen Feucht- und Waldgebieten, gute Lebensgrundlagen 
  für Amphibien.

  \section{GIS kurz erklärt}
  GIS bedeutet ''geographic information system'' oder auf Deutsch ''Geoinformationssystem''. Dies sind 
  Programme zur Betrachtung, Bearbeitung, Erstellung und Analyse von räumlichen Informationen. Es gibt verschiedenste Hersteller
  solcher Programme. In dieser Arbeit wird ein GIS mit namen ArcMap des Softwareherstellers ESRI verwendet.

  \section{Ziel und Aufbau der Arbeite}
  Zuerst werden die verwendeten Datensätzen beschrieben und erklärt. Danach wird aufgezeigt, welche Funktionen von ArcMap 
  verwendet wurden, wie die Datensätze verarbeitet wurden und wie schlussendlich konkrete Karten entstanden sind. Darauf werden die
  Resultate vorgestellt mit anschliessender Diskussion, Schlussfolgerung und Ausblick für die Zukunft.

